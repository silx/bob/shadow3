# shadow3

[![pipeline status](https://gitlab.esrf.fr/silx/bob/shadow3/badges/master/pipeline.svg)](https://gitlab.esrf.fr/silx/bob/shadow3/pipelines)

Build binary packages for [shadow3](https://github.com/oasys-kit/shadow3): [Artifacts](https://silx.gitlab-pages.esrf.fr/bob/shadow3)
