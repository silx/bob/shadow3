from setuptools._distutils import cygwinccompiler
import shutil
import os

shutil.copyfile(
    os.path.join(os.path.dirname(__file__), "cygwinccompiler_patched.py"),
    cygwinccompiler.__file__
)
